# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))
import org2sphinx
def setup(app):
    app.add_source_suffix('.org', 'org2sphinx')
    app.add_source_parser(org2sphinx.OrgParser)
    app.add_config_value('org_parser_config', {}, True)
    

# -- Project information -----------------------------------------------------

project = 'OER-Kurse'
copyright = '2022, Peter'
html_title = ""
html_logo = "_static/logo-inkubator-image.webp"

author = 'Peter'

# The full version, including alpha/beta/rc tags
release = '1'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx_sitemap'
]

html_baseurl = 'https://oer-kurse.gitlab.io/'

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# The language for content autogenerated by Sphinx. Refer to documentation
# for a list of supported languages.
#
# This is also used if you do content translation via gettext catalogs.
# Usually you set "language" from the command line for these cases.
#language = 'de'

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'furo'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

html_theme_options = {

    "light_css_variables": {
        "color-sidebar-background": "palegoldenrod",
        "color-sidebar-link-text--top-level": "#111",
        "color-background-secondary": "palegoldenrod",
    },
    "dark_css_variables": {
        "color-sidebar-background": "#111",
        "color-sidebar-link-text--top-level": "#DDD",
        "color-background-secondary": "#111",
    },
}

html_show_sourcelink = False

html_css_files = [
    'custom.css',
#    '../Berlin-Clock/staticBerlin-Clock.css'
]

html_js_files = [
    'Berlin-Clock/Berlin-Clock.js',
    'Berlin-Clock/v3d.js',
]
html_baseurl = 'https://oer-kurse.gitlab.io/'
sitemap_filename = "sitemap.xml"
